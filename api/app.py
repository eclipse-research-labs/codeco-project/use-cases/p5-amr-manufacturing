# SPDX-FileCopyrightText: 2024 fortiss GmbH
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: Hongyu Zhu, fortiss

from flask import Flask, render_template, request, redirect, url_for
from kubernetes import client, config
import yaml
import os
import json
from prometheus_api_client import PrometheusConnect
from flask_socketio import SocketIO
import time

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'

# Kubernetes set up
config.load_kube_config()

prom = PrometheusConnect(url="http://localhost:9090", disable_ssl=True)

def create_pod_from_yaml(yaml_file_path):
    with open(yaml_file_path) as f:
        docs = yaml.safe_load_all(f)
        for doc in docs:
            if isinstance(doc, dict) and doc:
                try:
                    if doc['kind'] == 'Deployment':
                        apps_v1 = client.AppsV1Api()
                        apps_v1.create_namespaced_deployment(body=doc, namespace="default")
                except client.rest.ApiException as e:
                    print("Exception when calling Kubernetes API: %s\n" % e)

@app.route('/', methods=['GET'])
def index():
    v1 = client.CoreV1Api()
    specific_nodes = ['master', 'turtlebot1', 'turtlebot2']
    nodes_info = []
    namespaces = ["monitoring", "default"]

    for node_name in specific_nodes:
        try:
            node = v1.read_node(name=node_name)
            pods = v1.list_pod_for_all_namespaces(field_selector=f'spec.nodeName={node.metadata.name}').items
            pods_info = []
            for pod in pods:
                if pod.metadata.namespace in namespaces:  
                    pod_status = pod.status.phase
                    pods_info.append({"pod_name": pod.metadata.name, "namespace": pod.metadata.namespace, "status": pod_status})

            node_info = {
                "node_name": node.metadata.name,
                "status": "Ready" if all(condition.status == "True" for condition in node.status.conditions if condition.type == "Ready") else "Not Ready",
                "pods": pods_info
            }
            nodes_info.append(node_info)

        except client.rest.ApiException as e:
            print(f"Exception when calling Kubernetes API for node {node_name}: {e}\n")

    containers = ["turtlebot-bringup", "pc-nav", "pc-slam"]
    pod_latencies = []

    for container in containers:
        query = f'sum(rate(container_cpu_usage_seconds_total{{container="{container}"}}[5m])) by (pod, container)'
        results = prom.custom_query(query=query)

        
        print(f"Results for container {container}: {results}")

        for result in results:
            if 'metric' in result and 'container' in result['metric']:
                pod_name = result['metric']['pod']
                container_name = result['metric']['container']
                latency = result['value'][1]
                pod_latencies.append({"pod": pod_name, "container": container_name, "latency": latency})
            else:
                print(f"Missing 'container' field in result: {result}")

    return render_template('index.html', nodes=nodes_info, pod_latencies=pod_latencies)


@app.route('/deploy', methods=['POST'])
def deploy():
    node = request.form['node']
    workload = request.form['workload']
    yaml_filename = f"{node}_{workload}.yaml"
    yaml_file_path = os.path.join(app.root_path, 'yaml', yaml_filename)
    create_pod_from_yaml(yaml_file_path)
    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=5000)
