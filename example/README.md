# 1. Apply application via CODECO framework 

Some basic kubernetes commands:
```
    kubectl get pod --all-namespaces -o wide
    kubectl get deployments
    kubectl describe pod kepler-exporter-9zt7d -n kepler
    kubectl get nodes --show-labels
```
Issues:

L2SM-operator deploy, we must ensure that master node has been labeled as below:
```
     kubectl label nodes kind-control-plane dedicated=control-plane
```

kind cluster 'too many open files' caused by running out of inotify recources.

To increase these limits temporarily run the following commands on the host:
```
    sudo sysctl fs.inotify.max_user_watches=524288
    sudo sysctl fs.inotify.max_user_instances=512
```
Or make it persistently by adding these lines to '/etc/sysctl.conf':
```
    fs.inotify.max_user_watches = 524288
    fs.inotify.max_user_instances = 512
```
Each node must set up CNI bridge:
```
    sudo mkdir -p /tmp/plugins/bin
    sudo curl -L -o /tmp/plugins/bin/cni-plugins.tgz https://github.com/containernetworking/plugins/releases/download/v1.3.0/cni-plugins-linux-amd64-v1.3.0.tgz``
    sudo tar -xzvf /tmp/plugins/bin/cni-plugins.tgz -C /tmp/plugins/bin/
```

## 1.1. Install CODECO all components via acm

Launch lens:
```
    lens-desktop
```
Install Multus CNI:
```
    kubectl apply -f https://raw.githubusercontent.com/k8snetworkplumbingwg/multus-cni/master/deployments/multus-daemonset-thick.yml
```

### 1.1.1. Get the ACM code
```
    git clone https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/acm.git
```

### 1.1.2. Build and deploy ACM
```
    make docker-build docker-push IMG=zhuhongyuy/codecoapp-operator:v3.0
    make deploy IMG=zhuhongyuy/codecoapp-operator:v3.0
```
#### 1.1.2.1. After netma deploy (under acm):
```
    chmod +x /home/zhu/Desktop/codeco/acm/scripts/after_netma_deployment.sh
    /home/zhu/Desktop/codeco/acm/scripts/after_netma_deployment.sh
```

### 1.1.3. Create a cluster:
```
    kind create cluster --config kind-config.yaml
```
### 1.1.4. Prometheus and Grafana Access:
```
    kubectl port-forward service/prometheus-k8s --namespace=monitoring 39999:9090
    kubectl port-forward service/grafana --namespace=monitoring 38888:3000
```
### 1.1.5. Assignment Plan CR by SWM:
```
    kubectl get assignmentplans.qos-scheduler.siemens.com -A -o yaml
```
### 1.1.6. Netma-topology CR:
```
    kubectl get netma-topologies.codeco.com -n he-codeco-netma netma-sample -o yaml
    kubectl get l2networks -A
    kubectl get networktopology -A
```

## 2.1 Delete Application:
```
    kubectl delete application acm-swm-app -n he-codeco-acm
    kubectl delete codecoapp codecoappinstance3 -n he-codeco-acm
    kubectl delete applicationgroup acm-applicationgroup -n he-codeco-acm
```
### 2.1.1 Delete application k8s:
```
    kubectl delete deployment listener
    kubectl delete deployment talker
```

## 3.1 Apply Application:
```
    kubectl create -f ros2_pubsub.yaml
```
## 3.2 Application Logs:
```
    kubectl logs acm-swm-app-ros2-talker -n he-codeco-acm
    kubectl logs acm-swm-app-ros2-listener -n he-codeco-acm
    kubectl describe pod acm-swm-app-ros2-talker -n he-codeco-acm
    kubectl describe pod acm-swm-app-ros2-listener -n he-codeco-acm
```
## 3.3 Network exposure:
```
    kubectl delete -f 02_nemesys-deployment.yaml
    kubectl create -f 02_nemesys-deployment.yaml
```
