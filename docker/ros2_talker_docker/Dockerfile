# SPDX-FileCopyrightText: 2023 fortiss GmbH
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: Hongyu Zhu, fortiss

FROM arm64v8/ros:humble-ros-base
SHELL ["/bin/bash", "-c"]

ENV DEBIAN_FRONTEND noninteractive
ENV ROS_DISTRO humble
ENV RMW_IMPLEMENTATION=rmw_fastrtps_cpp
ENV ROS_DOMAIN_ID=30


RUN apt-get update && apt-get install -y --no-install-recommends python3-pip python3-colcon-common-extensions \
 && pip install setuptools==58.2.0 \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*

 RUN apt-get update && apt-get install -y \
    python3-argcomplete \
    python3-colcon-common-extensions \
    libboost-system-dev \
    build-essential \
    libudev-dev \
    ros-humble-demo-nodes-cpp \
    ros-humble-demo-nodes-py \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

CMD ["bash", "-c", "source /opt/ros/$ROS_DISTRO/setup.bash && ros2 run demo_nodes_cpp talker"]