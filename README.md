# CODECO P5: Automated Mobile Robot Use-case 

The CODECO P5 use-cases has as basis a design where Automated Mobile Robots expected to handle goods within a factory warehouse are being subject to remote control. CODECO as orchestration framework is expected to be use to assist in a better distribution of the overall a.pplication workload required for AMRs /AGVs to handle they tasks, reducing the required energy expenditure and eventually improving latency (time to completion).
The use-case is described in the [CODECO D8 deliverable](https://zenodo.org/records/8143800).


The code here presented corresponds to the Application Workload used in the use-case which is being developed in the [fortiss IIoT Lab](https://www.fortiss.org/forschung/fortiss-labs/detail/iiot-lab).

The topology currently being deployed in the lab is represented in the next Figure.

<img with=600px src="./Graph.png">

The hardware configuration of the CODECO testbed is as follows:
- Kubernetes master node, Lenovo ThinkPad X280
- Worker nodes, Raspberry Pis 4 
- Worker nodes, Turtlebots 3 burger

In terms of software, the following configuration is available:
- Ubuntu 22.04 for the Turtlebots
- Raspbian GNU/Linux 11 for the RPIs
- ROS2 humble for the Turtlebots



# 1. Setup
The setup has been developed based on the [Turtlebot 3 guidelines](https://turtlebot.github.io/turtlebot4-user-manual/setup/basic.html).

The explanation here is based on the "User Setup".

## 1.1 PC Setup
Requirements: 
- Ubuntu 22.04 LTS Desktop
- ROS2 Humble [Debian package](https://docs.ros.org/en/humble/Installation/Ubuntu-Install-Debians.html) installation is recommended.

Start the installation of the required ROS2 packages:

1. Install Gazebo
```
  sudo apt install ros-humble-gazebo-*
```
2. Install Cartographer
```
  sudo apt install ros-humble-cartographer
  sudo apt install ros-humble-cartographer-ros
```
3. Install Navigation2
```
  sudo apt install ros-humble-navigation2
  sudo apt install ros-humble-nav2-bringup
```
Install the Turtlebot packages from Source:


```
  sudo apt remove ros-humble-turtlebot3-msgs
  sudo apt remove ros-humble-turtlebot3

  git clone https://github.com/callmekarl/Turtlebot3_ws.git
  colcon build --symlink-install
  echo 'source ~/Turtlebot3_ws/install/setup.bash' >> ~/.bashrc
  source ~/.bashrc
```
Then, config the ROS environment:
```
  echo 'export ROS_DOMAIN_ID=30 #TURTLEBOT3' >> ~/.bashrc
  source ~/.bashrc
```

## 1.2 Turtlebot(Raspberry Pi) Setup
Requirements:
- Ubuntu server 22.04.5 LTS (64-bit)
- sshd

You can now access the turtlebot remotely via ssh:

```
ssh your_user@turtlebot_ip_address
```

Install ROS2 Humble on Turtlebot, following the [Turtlebot guidelines](https://turtlebot.github.io/turtlebot4-user-manual/setup/basic.html#installing-ros-2)

Then, install and build the required ROS packages:
```
  sudo apt install python3-argcomplete python3-colcon-common-extensions libboost-system-dev build-essential
  sudo apt install ros-humble-hls-lfcd-lds-driver
  sudo apt install ros-humble-turtlebot3-msgs
  sudo apt install ros-humble-dynamixel-sdk
  sudo apt install libudev-dev
  git clone https://github.com/callmekarl/Turtlebot3_ws.git
  echo 'source /opt/ros/humble/setup.bash' >> ~/.bashrc
  source ~/.bashrc
  colcon build --symlink-install --parallel-workers 1
  echo 'source ~/Turtlebot3_ws/install/setup.bash' >> ~/.bashrc
  source ~/.bashrc
```

Set the USB port for OpenCR:

```
  sudo cp `ros2 pkg prefix turtlebot3_bringup`/share/turtlebot3_bringup/script/99-turtlebot3-cdc.rules /etc/udev/rules.d/
  sudo udevadm control --reload-rules
  sudo udevadm trigger
```

Set the config ROS environment:


```
  echo 'export ROS_DOMAIN_ID=30 #TURTLEBOT3' >> ~/.bashrc
  source ~/.bashrc
```

Set the LDS configuration:

```
  echo 'export LDS_MODEL=LDS-02' >> ~/.bashrc
  source ~/.bashrc
```

Your Turtlebot is now set up with ROS2.

# 2. Docker Setup for Turtlebot
For managing Turtlebot3 Burgers controlled via Raspberry Pi and coordinating with remote PCs, Docker images serve as a powerful tool to quickly build and deploy the necessary applications. This approach significantly streamlines the development and deployment process, ensuring that both the Turtlebot3 Burgers and the remote PCs can efficiently run the required software environments with minimal setup.

## 2.1 ROS2 communication based on ARM
For testing ROS 2 Humble communications on a Raspberry Pi running Ubuntu, our pre-configured Docker image can be directly utilized. 

- **Pre-configured ROS_DOMAIN_ID**: The Docker image comes with a predefined `ROS_DOMAIN_ID`, ensuring compatibility and enabling straightforward communication between instances.

### 2.1.1 ROS2_talker
   Open a terminal and execute the following command to pull the Docker image:
   ```bash
   sudo docker pull zhuhongyuy/ros2_talker:v1
   ```

### 2.1.2 ROS2_listener
   Open a terminal and execute the following command to pull the Docker image:
   ```bash
   sudo docker pull zhuhongyuy/ros2_listener:v1
   ```

## 2.2 Docker images for Turtlebot bringup based on arm achitechture
In the Fortiss Lab, our Turtlebot3 Burgers are equipped with two different models of Lidars, namely LDS-01 and LDS-02. To accommodate these variations and streamline the deployment process, we have prepared two separate Docker images tailored to each Lidar model. This ensures that each Turtlebot3 Burger is automatically set up with the appropriate configuration for its specific Lidar model.

Depending on the Lidar model equipped on your Turtlebot3 Burger, you will need to pull the corresponding Docker image as follows:

**For Turtlebot3 Burger with LDS-01 Lidar**:
  Use the Docker image specifically configured for the LDS-01 model. This image contains all necessary configurations and dependencies tailored to the LDS-01 Lidar.
  ```bash
  sudo docker pull zhuhongyuy/turtlebot1:v7
  ```
**For Turtlebot3 Burger with LDS-02 Lidar**:
  Use the Docker image specifically configured for the LDS-01 model. This image contains all necessary configurations and dependencies tailored to the LDS-02 Lidar.
  ```bash
  sudo docker pull zhuhongyuy/turtlebot2:v3
  ```
## 2.3 Docker Images for Remote PC Applications
Given the limited computational resources of the Raspberry Pi, we adopt a strategy that offloads complex computations to a remote PC. This approach utilizes the communication capabilities of ROS2 nodes for efficient information exchange between the Raspberry Pi and the remote PC. Consequently, we can leverage the superior computing power of the remote PC to handle intensive tasks while maintaining the responsiveness and mobility of the Turtlebot3. Below, we detail the Docker images designed for running various applications on the remote PC to support this distributed computing architecture.

### 2.3.1 Docker images for Remote PC launch Teleoperation
  To conduct a Teleoperation task, the corresponding Docker image can be pulled and run using the following command:
   ```bash
   sudo docker pull zhuhongyuy/pc_teleop:v3
   ```

### 2.3.2 Docker images for Remote PC launch SLAM
  To conduct a SLAM task, the corresponding Docker image can be pulled and run using the following command:
   ```bash
   sudo docker pull zhuhongyuy/pc_slam:v1
   ```

### 2.3.3 Docker images for Remote PC launch Navigation
  To conduct a Navigation task, the corresponding Docker image can be pulled and run using the following command:
   ```bash
   sudo docker pull zhuhongyuy/pc_nav:v4
   ```

#  3. Running the CODECO P5 Application Workloads manually

Requirements:
- Docker and Docker compose, which you can get here: [docker downloads](https://docs.docker.com/engine/install/ubuntu/)


## 3.1 Clone Repository 
```
  git clone https://git.fortiss.org/iiot/codeco/applications/Turtlebot3_ws.git (internal to fortiss)

  git clone [https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/use-cases/p5-amr-manufacturing.git](https://gitlab.eclipse.org/eclipse-research-labs/codeco-project/use-cases/p5-amr-manufacturing.git) (public)

  cd Turtlebot3_ws 
```

## 3.2 Build Base Container
```
  sudo docker compose build 
```
Subsequently, it is essential to authorize the Docker container for running graphical user interfaces (GUIs).
```
  xhost +local:docker
```
## 3.3 Run Container 
```
  sudo docker compose run pc $@ 
```
## 3.4 Connect with Turtlebot and Launch the Bringup
```
  ssh your_user@turtlebot_ip
  export TURTLEBOT3_MODEL=burger
  ros2 launch turtlebot3_bringup robot.launch.py
  
```
## 3.5 Run different Application Containers 

### 3.5.1 Teleoperation
Open a new terminal, and run
```
  sudo docker compose run pc $@ 
  ros2 run turtlebot3_teleop teleop_keyboard
```
### 3.5.2 SLAM
Open a new terminal, and run
```
  sudo docker compose run pc $@ 
  ros2 launch turtlebot3_cartographer cartographer.launch.py
```
### 3.5.3 Navigation
Open a new terminal, and run
```
  sudo docker compose run pc $@ 
  ros2 launch turtlebot3_navigation2 navigation2.launch.py maps:=~/map.yaml
```

## 3.6 Check Container Metrices
```
  sudo docker stats
```


# 4. Running the Application
Nowadays, AMRs/AGVs are supporting the heterogeneous and growing demand of material handling and logistics in flexible factory environments. In our Use-case design, Turtlebot is configured to perform three workloads tasks respectively.

<img with=600px src="./workload.png">

- Workload1: AGV departs from Docking Station, picking up at Station 1, and then returns to the Docking Station to drop off. 
- Workload2: AGV departs from Docking Station, picking up at Station 3, and then returns to the Docking Station to drop off.
- Workload3: AGV departs from Docking Station, picking up at Station 2, droping off at Station 3, and then returns to the Docking Station.

### 4.1 Launch the Bringup
```
  ssh your_ip@agv_controller
  export TURTLEBOT3_MODEL=burger
  ros2 launch turtlebot3_bringup robot.launch.py
```
### 4.2 Launch the Navigation from the PC
```
  source ~/Turtlebot3_ws/install/setup.bash
  export TURTLEBOT3_MODEL=burger
  ros2 launch turtlebot3_navigation2 navigation2.launch.py map:=$HOME/map.yaml
```
### 4.3 Run the Workload from the PC
```
  source ~/Turtlebot3_ws/install/setup.bash
  export TURTLEBOT3_MODEL=burger
  python3 ~/Turtlebot3_ws/src/workload/scripts/workload1.py
```

# 4. Running the Stateful Application based on CODECO framework

This part provides a step-by-step guide to set up a virtual cluster using kind, deploy the CODECO framework, and run a stateful task migration application for AGVs. The application enables cold migration of a SLAM task from one AGV to another when the original AGV shuts down, preserving the constructed map state.

## 4.1 Prerequisites

### 4.1.1 Install NFS Server(based on Linux)
``` 
    sudo apt-get update
    sudo apt-get install nfs-kernel-server
    sudo mkdir -p /srv/nfs/kubedata
    sudo chown nobody:nogroup /srv/nfs/kubedata
```
### 4.1.2 Enable GUI Access
``` 
    xhost +
```
### 4.1.3 Create a Kind Cluster
``` 
    kind create cluster --config /example/kind-cluster-config.yaml
```
### 4.1.4 Deploy CODECO Framework
``` 
    make deploy IMG=<some-registry>/controller:latest
```
## 4.2 Apply Cold Migration Application

### 4.2.1 Deploy PV and PVC for Application
``` 
    kubectl apply -f /example/CODECO_SLAM/codeco_pv.yaml
```

### 4.2.2 Deploy Application
``` 
    kubectl apply -f /example/CODECO_SLAM/codeco_slam.yaml
```
### 4.2.3 Cordon and Drain the node to trigger migration
```
    kubectl cordon c1
    kubectl drain c1 --ignore-daemonsets --delete-local-data
```

## 4.3 Delete CODECO Application
```
    kubectl delete application acm-swm-app -n he-codeco-acm
    kubectl delete codecoapp codecoappinstance3 -n he-codeco-acm
    kubectl delete applicationgroup acm-applicationgroup -n he-codeco-acm
```


# LICENSE
Code and documentation copyright 2024 fortiss GmbH, under the MIT license.
Documentation is licensed under CC.

# Credits and Acknowledgements
Credits and Acknowledgements go to the fortiss team involved in the development of CODECO.

The application workload is being developed by Hongyu Zhu.