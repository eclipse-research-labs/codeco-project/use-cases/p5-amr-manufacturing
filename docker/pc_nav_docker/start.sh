# SPDX-FileCopyrightText: 2023 fortiss GmbH
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: Hongyu Zhu, fortiss

#!/bin/bash
source /opt/ros/humble/setup.bash
source /Turtlebot3_ws/install/setup.bash
ros2 launch turtlebot3_navigation2 navigation2.launch.py map:=/Turtlebot3_ws/maps/map.yaml