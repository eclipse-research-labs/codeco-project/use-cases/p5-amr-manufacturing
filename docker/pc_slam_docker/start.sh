# SPDX-FileCopyrightText: 2023 fortiss GmbH
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: Hongyu Zhu, fortiss

#!/bin/bash
source /opt/ros/humble/setup.bash
source /Turtlebot3_ws/install/setup.bash
ros2 launch turtlebot3_cartographer cartographer.launch.py

ros2 run turtlebot3_teleop teleop_keyboard